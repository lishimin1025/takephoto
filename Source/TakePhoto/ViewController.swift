//
//  ViewController.swift
//  TakePhoto
//
//  Created by common on 6/2/15.
//  Copyright (c) 2015 lishimin. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var feedView: UIImageView!
    var stillImageOutput: AVCaptureStillImageOutput?
    var session: AVCaptureSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureCamera()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureCamera() -> Bool {
        var captureDevice: AVCaptureDevice?
        var devices: NSArray = AVCaptureDevice.devices()
        
        for device: AnyObject in devices {
            if device.position == AVCaptureDevicePosition.Back {
                captureDevice = device as? AVCaptureDevice
            }
        }
        
        if captureDevice != nil {
            println(captureDevice!.localizedName)
            println(captureDevice!.modelID)
        } else {
            return false
        }
        
        var error: NSErrorPointer = nil
        var deviceInput: AVCaptureInput = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: error) as! AVCaptureDeviceInput
        
        self.stillImageOutput = AVCaptureStillImageOutput()
        
        self.session = AVCaptureSession()
        self.session?.sessionPreset = AVCaptureSessionPresetPhoto
        self.session?.addInput(deviceInput as AVCaptureInput)
        
        self.session?.addOutput(self.stillImageOutput)
        
        
        // layer for preview
        var previewLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer.layerWithSession(self.session) as! AVCaptureVideoPreviewLayer
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.frame = self.feedView.bounds
        
        self.feedView.layer.addSublayer(previewLayer)
        
        self.session?.startRunning()
        return true
    }
    
    @IBAction func takePhotoClicked(sender: UIButton) {
        
        // we do this on another thread so that we don't hang the UI
        
        let connection = self.stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo)
        
        self.stillImageOutput?.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: {
            (imageDataSampleBuffer, error) -> Void in
            if error == nil {
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                let attachments:NSDictionary = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, imageDataSampleBuffer, CMAttachmentMode(kCMAttachmentMode_ShouldPropagate)).takeUnretainedValue()
                if let image = UIImage(data: imageData) {
                   UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
                }
            }
            
        })
    }
    
    @IBAction func viewPhotoClicked(sender: UIButton) {
        let pickerC = UIImagePickerController()
        pickerC.mediaTypes = [kUTTypeImage as NSString, kUTTypeMovie as NSString]
        pickerC.delegate = self
        self.presentViewController(pickerC, animated: true, completion: nil)
    }
    

}

